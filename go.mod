module gitlab.com/golang-sftp-sample

require (
	github.com/kr/fs v0.1.0 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/pkg/sftp v1.10.0
	golang.org/x/crypto v0.0.0-20190411191339-88737f569e3a
)
